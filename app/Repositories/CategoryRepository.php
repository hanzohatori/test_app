<?php


namespace App\Repositories;


use App\Http\Requests\Categories\StoreCategoryRequest;
use App\Http\Resources\Categories\CategoryResource;
use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function storeCategory(StoreCategoryRequest $request)
    {
        return new CategoryResource(Category::create($request->validated()));
    }

    public function deleteCategory(Category $category)
    {
        if (!$category->existingProductCategories($category)) {
            $category->delete();
            return response([
                'message' => 'Category successfully deleted'
            ], 200);
        }

        return response([
            'message' => 'Category can not be delete, reason: linked to Product'
        ], 409);
    }
}
