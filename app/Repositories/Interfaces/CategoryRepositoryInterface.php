<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Categories\StoreCategoryRequest;
use App\Models\Category;

interface CategoryRepositoryInterface
{
    public function storeCategory(StoreCategoryRequest $request);

    public function deleteCategory(Category $category);
}
