<?php


namespace App\Services;


class MediaService
{
    public static function processImage($value)
    {
        return asset('/storage/'.str_replace('public/','',$value->store('public/avatars')));
    }
}
