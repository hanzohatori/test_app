<?php

namespace App\Models;

use App\Contracts\CategoryContract;
use App\Contracts\ProductCategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = CategoryContract::FILLABLE;

    public function existingProductCategories(Category $category)
    {
        return ProductCategory::where(ProductCategoryContract::CATEGORY_ID, $category->id)->exists();
    }
}
