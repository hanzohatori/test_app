<?php

namespace App\Models;

use App\Contracts\ProductContract;
use App\Services\MediaService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ProductContract::FILLABLE;

    public function setImageAttribute($value)
    {
        $this->attributes[ProductContract::IMAGE] = MediaService::processImage($value);
    }

    public function productsCategories()
    {
        return $this->belongsToMany(Category::class, 'product_categories',
            'product_id', 'category_id');
    }
}
