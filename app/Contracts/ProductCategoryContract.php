<?php


namespace App\Contracts;


interface ProductCategoryContract
{
    const PRODUCT_ID = 'product_id';
    const CATEGORY_ID = 'category_id';

    const FILLABLE = [
        self::PRODUCT_ID,
        self::CATEGORY_ID
    ];
}
