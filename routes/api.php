<?php

use App\Http\Controllers\Api\Categories\CategoryController;
use App\Http\Controllers\Api\Products\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'products' => ProductController::class,
    'categories' => CategoryController::class
]);

Route::get('/attach/{category}/{product}', [ProductController::class, 'attach']);
Route::get('/detach/{category}/{product}', [ProductController::class, 'detach']);
