<?php

namespace Tests\Feature;

use App\Contracts\ProductContract;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_update_product()
    {
        $product = Product::inRandomOrder()->first();
        $response = $this->withHeaders(
            [
                'Accept' => 'application/json'
            ]
        )->put('/api/products/'.$product->id, [
            ProductContract::NAME => Str::random(10),
            ProductContract::DESCRIPTION => 'kek',
            ProductContract::IMAGE => UploadedFile::fake()->image('avatar.jpg'),
            ProductContract::PUBLISHED => 1,
            ProductContract::PRICE => mt_rand(10000,50000)
        ]);

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_store_product()
    {
        $response = $this->withHeaders(
            [
                'Accept' => 'application/json'
            ]
        )->post('/api/products', [
            ProductContract::NAME => Str::random(10),
            ProductContract::DESCRIPTION => 'kek',
            ProductContract::IMAGE => UploadedFile::fake()->image('avatar.jpg'),
            ProductContract::PUBLISHED => 1,
            'category_ids' => [
                1,
                2
            ],
            ProductContract::PRICE => mt_rand(10000,50000)
        ]);


        $response->assertStatus(201);
    }
}
